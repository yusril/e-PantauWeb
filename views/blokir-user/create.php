<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserBlock */

$this->title = 'Create User Block';
$this->params['breadcrumbs'][] = ['label' => 'User Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-block-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
