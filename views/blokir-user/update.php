<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserBlock */

$this->title = 'Update User Block: ' . $model->id_block;
$this->params['breadcrumbs'][] = ['label' => 'User Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_block, 'url' => ['view', 'id' => $model->id_block]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-block-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
