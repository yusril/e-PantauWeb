<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\NewAppAsset;
use app\models\User;
use ramosisw\CImaterial\web\MaterialAsset;

MaterialAsset::register($this);
$this->registerJsFile("@web/js/notification-node.js", ['depends' => 'yii\web\JqueryAsset']); //depends -> loaded after jquery.js
$this->registerJsFile("https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js");
function ActiveTest($route)
{
    if (strpos(Yii::$app->requestedRoute, $route) !== false ) {
        return 'class="active"';
    } else return null;
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<style>
       #map {
        height: 250px;
        width: 600px;
       }
</style>
<body>
<?php $this->beginBody() ?>

<div class="wrap wrapper">
    <?php if(!Yii::$app->user->isGuest): ?>
    <div class="sidebar" data-color="red" data-image="<?= Yii::getalias('@web') ?>/img/sidebar.png">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
        -->
            <div class="logo">
                <a href="" class="simple-text">
                    E–Pantau
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <?php if(User::findLevelById(Yii::$app->user->getId()) == 1): ?>
                    <li <?= ActiveTest('lokasi-darurat') ?>>
                        <a href="<?= Url::to(['lokasi-darurat/index']) ?>">
                            <i class="material-icons">location_on</i>
                            <p>Lokasi Darurat</p>
                        </a>
                    </li>
                    <!-- <li <?= ActiveTest('site/index') ?>>
                        <a href="<?= Url::to(['site/index']) ?>">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li> -->
                    <?php endif; ?>

                    <?php if(User::findLevelById(Yii::$app->user->getId()) == 2): ?>
                    <li <?= ActiveTest('hapus-post') ?>>
                        <a href="<?= Url::to(['hapus-post/index']) ?>">
                            <i class="material-icons">report</i>
                            <p>Hapus Post</p>
                        </a>
                    </li>
                    <!-- <li <?= ActiveTest('blokir-user') ?>>
                        <a href="<?= Url::to(['blokir-user/index']) ?>">
                            <i class="material-icons">block</i>
                            <p>Blokir User</p>
                        </a>
                    </li> -->
                    <?php endif; ?>
                    
                    <li <?= ActiveTest('site/about') ?>>
                        <a href="<?= Url::to(['site/about']) ?>">
                            <i class="material-icons">person</i>
                            <p>About</p>
                        </a>
                    </li>

                    <li>
                        
                        <a href="#" onclick="logout.submit();">
                            <i class="material-icons">exit_to_app</i>
                            <p>Logout</p>
                        </a>
                    <!-- Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    ) -->
                        <?= Html::beginForm(['/site/logout'], 'post', ['id' => "logout"]) ?>
                        <?= Html::endForm() ?>
                    </li>
                    <!-- <li>
                        <a href="./icons.html">
                            <i class="material-icons">bubble_chart</i>
                            <p><?= Yii::$app->requestedRoute ?></p>
                        </a>
                    </li> -->
                    <!-- <li>
                        <form action="<?= Url::to(['site/logout']) ?>"
                        <a href="./notifications.html">
                            <i class="material-icons text-gray">notifications</i>
                            <p>Notifications</p>
                        </a>
                    </li> -->
                </ul>
            </div>
    </div>
    <?php endif; ?>
    <div class="main-panel">
        <?php
        NavBar::begin([
            'brandLabel' => $this->title,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-transparent navbar-absolute',
            ],
            'innerContainerOptions' => [
                'class' => 'container-fluid',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                // ['label' => 'Home', 'url' => ['/site/index']],
                // ['label' => 'About', 'url' => ['/site/about']],
                // ['label' => 'Contact', 'url' => ['/site/contact']],
                Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                ) : ( ""
                    // '<li>'
                    // . Html::beginForm(['/site/logout'], 'post')
                    // . Html::submitButton(
                    //     'Logout (' . Yii::$app->user->identity->username . ')',
                    //     ['class' => 'btn btn-link logout']
                    // )
                    // . Html::endForm()
                    // . '</li>'
                )
            ],
        ]);
        NavBar::end();
        ?>

        <div class="content">
            <div class="container-fluid">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        <div class="content">

        <footer class="footer">
            <div class="container-fluid">
                <p class="pull-left">&copy; E-Pantau Team Developer <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>
    </div>
    
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
