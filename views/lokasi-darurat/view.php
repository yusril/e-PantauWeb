<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\UserNew;

$aContext = array(
    'http' => array(
        // 'proxy' => 'proxy3.pens.ac.id:3128',
        'request_fulluri' => true,
    ),
);
$cxContext = stream_context_create($aContext);
$json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$model->latitude.','.$model->longitude.'&key=AIzaSyCzqQVUqwMobrF8egv7JCpTXGL42f5EoJw');//, FALSE, $cxContext);
$obj = json_decode($json);
$user = UserNew::findIdentity($model->id_user);

$this->title = "Detail Lokasi Kejadian";
$this->params['breadcrumbs'][] = ['label' => 'Lokasi Darurat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id_lokasi_darurat;
?>
<div class="lokasi-darurat-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <!-- <?= Html::a('Update', ['update', 'id' => $model->id_lokasi_darurat], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_lokasi_darurat], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?> -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id_lokasi_darurat',
            // 'id_user',
            [
                'label' => 'Pengirim',
                'value' => $user->nama
            ],
            [
                'label' => 'No. Telepon',
                'value' => $user->telepon
            ],
            'longitude',
            'latitude',
            [
                'label' => 'Alamat',
                'value' => $obj->results[0]->formatted_address
            ],
            [
                'label' => 'Map', 
                'value' => '<div id="map"></div>',
                'format' => ['raw']
            ],
            'tanggal',
        ],
    ]) ?>

</div>
<script>

   function initMap() {
       var coordinate = {lat: <?= $model->latitude ?>, lng: <?= $model->longitude ?>};
       var geocoder = new google.maps.Geocoder;
       var infowindow = new google.maps.InfoWindow;
       var map = new google.maps.Map(document.getElementById('map'), {
           zoom: 15,
           center: coordinate
       });
       var marker = new google.maps.Marker({
           position: coordinate,
           map: map
       });

       geocoder.geocode({'location':coordinate}, function(results, status) {
        if (status === 'OK') {
          if (results[0]) {
            console.log(results);
            infowindow.setContent(results[0].formatted_address);
            infowindow.open(map, marker);
          } else {
            window.alert('No results found');
          }
        } else {
          window.alert('Geocoder failed due to: ' + status);
        }
      });
   };
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzqQVUqwMobrF8egv7JCpTXGL42f5EoJw&callback=initMap"></script>