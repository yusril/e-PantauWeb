<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LokasiDarurat */

$this->title = 'Update Lokasi Darurat: ' . $model->id_lokasi_darurat;
$this->params['breadcrumbs'][] = ['label' => 'Lokasi Darurats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_lokasi_darurat, 'url' => ['view', 'id' => $model->id_lokasi_darurat]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lokasi-darurat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
