<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LokasiDarurat */

$this->title = 'Create Lokasi Darurat';
$this->params['breadcrumbs'][] = ['label' => 'Lokasi Darurats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lokasi-darurat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
