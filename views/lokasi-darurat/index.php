<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LokasiDaruratSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lokasi Darurat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lokasi-darurat-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Lokasi Darurat', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
    <div class="card">
    <div class="card-header" data-background-color="red">
                <h3 class="title"><?= Html::encode($this->title) ?></h3>
            </div>
        <div class="card-content">
        
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id_lokasi_darurat',
                    'id_user',
                    'longitude',
                    'latitude',
                    'tanggal',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}'
                    ],
                ],
            ]); ?>
        </div>
    
    </div>
</div>
