<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LaporanUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hapus Post';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laporan-user-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Laporan User', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
    <div class="card">
    <div class="card-header" data-background-color="red">
                <h3 class="title"><?= Html::encode($this->title) ?></h3>
            </div>
        <div class="card-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id_laporan',
                'id_post',
                'id_terlapor',
                'id_pelapor',
                'tanggal',
                // 'isi',
                // 'status',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}'
                ],
            ],
        ]); ?>
        </div>
    </div>
</div>
