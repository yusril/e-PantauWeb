<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostKejadianSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-kejadian-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_post_kejadian') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'judul') ?>

    <?= $form->field($model, 'tanggal_posting') ?>

    <?= $form->field($model, 'lokasi') ?>

    <?php // echo $form->field($model, 'gambar') ?>

    <?php // echo $form->field($model, 'caption') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
