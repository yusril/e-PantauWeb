<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PostKejadian */

$this->title = 'Create Post Kejadian';
$this->params['breadcrumbs'][] = ['label' => 'Post Kejadians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-kejadian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
