<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PostKejadian */

$this->title = 'Update Post Kejadian: ' . $model->id_post_kejadian;
$this->params['breadcrumbs'][] = ['label' => 'Post Kejadians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_post_kejadian, 'url' => ['view', 'id' => $model->id_post_kejadian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="post-kejadian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
