<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostKejadianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Post Kejadian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-kejadian-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post Kejadian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_post_kejadian',
            'id_user',
            'judul',
            'tanggal_posting',
            // 'lokasi',
            // 'gambar',
            // 'caption',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
</div>
