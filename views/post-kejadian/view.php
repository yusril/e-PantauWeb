<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PostKejadian */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Post Kejadian', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-kejadian-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_post_kejadian], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_post_kejadian], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_post_kejadian',
            'id_user',
            'judul',
            'tanggal_posting',
            // 'lokasi',
            'gambar',
            'caption',
        ],
    ]) ?>

</div>
