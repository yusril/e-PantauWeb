<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="site-about">
        <div class="card-header" data-background-color="red">
            <h3 class="title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="card-content">
        <p>
            This Application was made by E-Pantau Team Developer.
        </p>
        </div>

        <!-- <code><?= __FILE__ ?></code> -->
    </div>
</div>