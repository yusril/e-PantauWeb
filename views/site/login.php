<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use app\assets\NewAppAsset;
use ramosisw\CImaterial\web\MaterialAsset;

NewAppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Login Page</title>
    <?php $this->head() ?>
</head>
<body class="signup-page">
<?php $this->beginBody() ?>
<div class="wrapper">
	<div class="header header-filter" style="background-image: url('<?= Yii::getalias('@web') ?>/img/cover2.png'); background-size: cover; background-position: top center;">
		<div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="card card-signup site-login">
                        <?php $form = ActiveForm::begin([
                            'id' => 'login-form',
                            //'layout' => 'horizontal',
                            ]); ?>
                            <div class="header header-danger text-center">
                                <h4>E - Pantau: Sign In</h4>
                            </div>
                            <div class="content">

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">account_circle</i>
                                </span>
                                <!-- <input type="text" class="form-control" placeholder="Username..."> -->
                                <?= $form->field($model, 'username')->textInput(['autofocus' => true, "class" => "form-control", "placeholder" => "Username, Type Here...", "autocomplete" => "off"]) ?>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <!-- <input type="password" placeholder="Password..." class="form-control" /> -->
                                <?= $form->field($model, 'password')->passwordInput(["class" => "form-control", "placeholder" => "Password, Type Here..."]) ?>
                            </div>

                                <!-- If you want to add a checkbox to this form, uncomment this code

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="optionsCheckboxes" checked>
                                        Subscribe to newsletter
                                    </label>
                                </div> -->
                            </div>
                            <div class="footer text-center">
                                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-simple', 'name' => 'login-button']) ?>
                                <!-- <a href="#pablo" class="btn btn-simple btn-primary btn-lg">Get Started</a> -->
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

