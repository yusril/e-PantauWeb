<?php
use yii\helpers\Url;
use app\models\User;

if(User::findLevelById(Yii::$app->user->getId()) == 1)
    Yii::$app->response->redirect(Url::to(['lokasi-darurat/index'], true));
elseif (User::findLevelById(Yii::$app->user->getId()) == 2) {
    Yii::$app->response->redirect(Url::to(['hapus-post/index'], true));
}
?>