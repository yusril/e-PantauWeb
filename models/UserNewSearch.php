<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserNew;

/**
 * UserNewSearch represents the model behind the search form about `app\models\UserNew`.
 */
class UserNewSearch extends UserNew
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['nik', 'nama', 'alamat', 'telepon', 'username', 'password', 'foto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserNew::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user' => $this->id_user,
        ]);

        $query->andFilterWhere(['username'=> $this->username])
            ->andFilterWhere(['password'=> $this->password]);
            // ->andFilterWhere(['nik'=> $this->nik])
            // ->andFilterWhere(['nama'=> $this->nama])
            // ->andFilterWhere(['alamat'=> $this->alamat])
            // ->andFilterWhere(['telepon'=> $this->telepon])
            // ->andFilterWhere(['foto'=> $this->foto]);

        return $dataProvider;
    }
}
