<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_kejadian".
 *
 * @property integer $id_post_kejadian
 * @property integer $id_user
 * @property string $judul
 * @property string $tanggal_posting
 * @property string $lokasi
 * @property string $gambar
 * @property string $caption
 */
class PostKejadian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_kejadian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['tanggal_posting'], 'safe'],
            [['judul'], 'string', 'max' => 50],
            [['caption'], 'string', 'max' => 255],
            [['latitude', 'longitude'], 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_post_kejadian' => 'Id Post Kejadian',
            'id_user' => 'Id User',
            'judul' => 'Judul',
            'tanggal_posting' => 'Tanggal Posting',
            'gambar' => 'Gambar',
            'caption' => 'Caption',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude'
        ];
    }

    public function getUser()
    {
        return $this->hasOne(UserNew::className(), ['id_user' => 'id_user']);
    }

    public static function findById($id_post_kejadian)
    {
        return static::findOne(['id_post_kejadian' => $id_post_kejadian]);
    }

    public function extraFields()
    {
        return ['user'];
    }

    // public function init()
    // {
    //     $this->on(\yii\db\ActiveRecord::EVENT_AFTER_INSERT, function ($event) {
    
    //         // Category ID just created
    //         $id_post = $event->sender->id_post_kejadian;
    //         //var_dump(Yii::$app->request->post());
            
    //         $temp = Yii::$app->request->post('tagPost');
    //         $tag1 = $temp['tag1'];
    //         $tag2 = $temp['tag2'];
    //         $tag3 = $temp['tag3'];
    
    //         $tagpost = new TagPost;
    //         $tagpost->setAttribute('id_post', $id_post);
    //         $tagpost->setAttribute('tag1', $tag1);
    //         $tagpost->setAttribute('tag2', $tag2);
    //         $tagpost->setAttribute('tag3', $tag3);
    //         $tagpost->save();
    //     });

    //     $this->on(\yii\db\ActiveRecord::EVENT_AFTER_UPDATE, function ($event) {
        
    //         // Category ID just created
    //         $id_post = $event->sender->id_post_kejadian;
    //         // var_dump(Yii::$app->request->post());
            
    //         $temp = Yii::$app->request->post('tagPost');
    //         $tag1 = $temp['tag1'];
    //         $tag2 = $temp['tag2'];
    //         $tag3 = $temp['tag3'];
        
    //         $tagpost = TagPost::find()->where(["id_post" => $id_post])->one();
    //         $tagpost->setAttribute('tag1', $tag1);
    //         $tagpost->setAttribute('tag2', $tag2);
    //         $tagpost->setAttribute('tag3', $tag3);
    //         $tagpost->save();
    //     });
    // }
    

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getMempunyai3s()
    // {
    //     return $this->hasMany(Mempunyai3::className(), ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getIDKATEGORIs()
    // {
    //     return $this->hasMany(PostTags::className(), ['ID_KATEGORI' => 'ID_KATEGORI'])->viaTable('mempunyai_3', ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getIDUSER()
    // {
    //     return $this->hasOne(User::className(), ['ID_USER' => 'ID_USER']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getPostKomentars()
    // {
    //     return $this->hasMany(PostKomentar::className(), ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN']);
    // }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getPostLikes()
    // {
    //     return $this->hasMany(PostLike::className(), ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN']);
    // }
}
