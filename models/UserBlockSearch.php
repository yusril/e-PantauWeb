<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserBlock;

/**
 * UserBlockSearch represents the model behind the search form about `app\models\UserBlock`.
 */
class UserBlockSearch extends UserBlock
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_block', 'id_user', 'status'], 'integer'],
            [['tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserBlock::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_block' => $this->id_block,
            'id_user' => $this->id_user,
            'tanggal' => $this->tanggal,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
