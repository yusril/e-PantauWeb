<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_like".
 *
 * @property integer $ID_POST_LIKE
 * @property integer $ID_POST_KEJADIAN
 * @property integer $ID_USER
 *
 * @property PostKejadian $iDPOSTKEJADIAN
 * @property User $iDUSER
 */
class PostLike extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_like';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_POST_LIKE'], 'required'],
            [['ID_POST_LIKE', 'ID_POST_KEJADIAN', 'ID_USER'], 'integer'],
            [['ID_POST_LIKE'], 'unique'],
            [['ID_POST_KEJADIAN'], 'exist', 'skipOnError' => true, 'targetClass' => PostKejadian::className(), 'targetAttribute' => ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN']],
            [['ID_USER'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ID_USER' => 'ID_USER']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_POST_LIKE' => 'Id  Post  Like',
            'ID_POST_KEJADIAN' => 'Id  Post  Kejadian',
            'ID_USER' => 'Id  User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPOSTKEJADIAN()
    {
        return $this->hasOne(PostKejadian::className(), ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSER()
    {
        return $this->hasOne(User::className(), ['ID_USER' => 'ID_USER']);
    }
}
