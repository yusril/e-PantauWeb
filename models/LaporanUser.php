<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "laporan_user".
 *
 * @property integer $id_laporan
 * @property integer $id_post
 * @property integer $id_terlapor
 * @property integer $id_pelapor
 * @property string $tanggal
 * @property string $isi
 * @property integer $status
 */
class LaporanUser extends \yii\db\ActiveRecord
{
    public $jumlah_laporan;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laporan_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_post', 'id_terlapor', 'id_pelapor'], 'required'],
            [['id_post', 'id_terlapor', 'id_pelapor', 'status'], 'integer'],
            [['tanggal'], 'safe'],
            [['isi'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_laporan' => 'Id Laporan',
            'id_post' => 'Id Post',
            'id_terlapor' => 'Id Terlapor',
            'id_pelapor' => 'Id Pelapor',
            'tanggal' => 'Tanggal',
            'isi' => 'Isi',
            'status' => 'Status',
        ];
    }

    public function getTerlapor()
    {
        return $this->hasOne(UserNew::className(), ['id_user' => 'id_terlapor']);
    }

    public function getPelapor()
    {
        return $this->hasOne(UserNew::className(), ['id_user' => 'id_pelapor']);
    }

    public function getPost()
    {
        return $this->hasOne(PostKejadian::className(), ['id_post_kejadian' => 'id_post']);
    }

    public function extraFields()
    {
        return ['pelapor', 'terlapor', 'post'];
    }


    //delete post after this was deleted
    private $idCache; 

    public function beforeDelete()
    {
        $this->idCache = $this->id_post;

        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        $post = PostKejadian::findOne($this->idCache);
        $post->delete();

        $reports = LaporanUser::deleteAll(['id_post' => $this->idCache]);
        
        // $children = LaporanUser::model()->findAll($criteria);

        // foreach ($children as $child)
        // {
        //     $child->delete();
        // }

        parent::afterDelete();
    }
}
