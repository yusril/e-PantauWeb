<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tag_post".
 *
 * @property integer $id_tag
 * @property integer $id_post
 * @property string $tag1
 * @property string $tag2
 * @property string $tag3
 */
class TagPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_post'], 'required'],
            [['id_post'], 'integer'],
            [['tag1', 'tag2', 'tag3'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tag' => 'Id Tag',
            'id_post' => 'Id Post',
            'tag1' => 'Tag1',
            'tag2' => 'Tag2',
            'tag3' => 'Tag3',
        ];
    }
}
