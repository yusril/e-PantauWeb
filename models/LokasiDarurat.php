<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lokasi_darurat".
 *
 * @property integer $id_lokasi_darurat
 * @property integer $id_user
 * @property double $longitude
 * @property double $latitude
 * @property string $tanggal
 */
class LokasiDarurat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lokasi_darurat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['longitude', 'latitude'], 'number'],
            [['tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_lokasi_darurat' => 'Id Lokasi Darurat',
            'id_user' => 'Id User',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'tanggal' => 'Tanggal',
        ];
    }
}
