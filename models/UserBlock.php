<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_block".
 *
 * @property integer $id_block
 * @property integer $id_user
 * @property string $tanggal
 * @property integer $status
 */
class UserBlock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_block';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user', 'status'], 'integer'],
            [['tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_block' => 'Id Block',
            'id_user' => 'Id User',
            'tanggal' => 'Tanggal',
            'status' => 'Status',
        ];
    }
}
