<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_tags".
 *
 * @property integer $ID_KATEGORI
 * @property integer $ID_TAG
 *
 * @property Mempunyai3[] $mempunyai3s
 * @property PostKejadian[] $iDPOSTKEJADIANs
 * @property Tag $iDTAG
 */
class PostTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_KATEGORI'], 'required'],
            [['ID_KATEGORI', 'ID_TAG'], 'integer'],
            [['ID_KATEGORI'], 'unique'],
            [['ID_TAG'], 'exist', 'skipOnError' => true, 'targetClass' => Tag::className(), 'targetAttribute' => ['ID_TAG' => 'ID_TAG']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_KATEGORI' => 'Id  Kategori',
            'ID_TAG' => 'Id  Tag',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMempunyai3s()
    {
        return $this->hasMany(Mempunyai3::className(), ['ID_KATEGORI' => 'ID_KATEGORI']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPOSTKEJADIANs()
    {
        return $this->hasMany(PostKejadian::className(), ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN'])->viaTable('mempunyai_3', ['ID_KATEGORI' => 'ID_KATEGORI']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDTAG()
    {
        return $this->hasOne(Tag::className(), ['ID_TAG' => 'ID_TAG']);
    }
}
