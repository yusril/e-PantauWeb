<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_komentar".
 *
 * @property integer $ID_KOMENTAR
 * @property integer $ID_POST_KEJADIAN
 * @property integer $ID_USER
 * @property string $KOMENTAR
 *
 * @property PostKejadian $iDPOSTKEJADIAN
 * @property User $iDUSER
 */
class PostKomentar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_komentar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_KOMENTAR'], 'required'],
            [['ID_KOMENTAR', 'ID_POST_KEJADIAN', 'ID_USER'], 'integer'],
            [['KOMENTAR'], 'string', 'max' => 255],
            [['ID_KOMENTAR'], 'unique'],
            [['ID_POST_KEJADIAN'], 'exist', 'skipOnError' => true, 'targetClass' => PostKejadian::className(), 'targetAttribute' => ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN']],
            [['ID_USER'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ID_USER' => 'ID_USER']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_KOMENTAR' => 'Id  Komentar',
            'ID_POST_KEJADIAN' => 'Id  Post  Kejadian',
            'ID_USER' => 'Id  User',
            'KOMENTAR' => 'Komentar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDPOSTKEJADIAN()
    {
        return $this->hasOne(PostKejadian::className(), ['ID_POST_KEJADIAN' => 'ID_POST_KEJADIAN']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDUSER()
    {
        return $this->hasOne(User::className(), ['ID_USER' => 'ID_USER']);
    }
}
