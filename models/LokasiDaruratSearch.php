<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LokasiDarurat;

/**
 * LokasiDaruratSearch represents the model behind the search form about `app\models\LokasiDarurat`.
 */
class LokasiDaruratSearch extends LokasiDarurat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_lokasi_darurat', 'id_user'], 'integer'],
            [['longitude', 'latitude'], 'number'],
            [['tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LokasiDarurat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_lokasi_darurat' => $this->id_lokasi_darurat,
            'id_user' => $this->id_user,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'tanggal' => $this->tanggal,
        ]);

        return $dataProvider;
    }
}
