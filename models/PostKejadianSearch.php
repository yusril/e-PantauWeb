<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PostKejadian;

/**
 * PostKejadianSearch represents the model behind the search form about `app\models\PostKejadian`.
 */
class PostKejadianSearch extends PostKejadian
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_post_kejadian', 'id_user'], 'integer'],
            [['judul', 'tanggal_posting', 'lokasi', 'gambar', 'caption'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostKejadian::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSizeLimit' => [0, 10], //first array: minimal size, second array: per page size
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //$query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_post_kejadian' => $this->id_post_kejadian,
            'id_user' => $this->id_user,
            'tanggal_posting' => $this->tanggal_posting,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'gambar', $this->gambar])
            ->andFilterWhere(['like', 'caption', $this->caption]);

        return $dataProvider;
    }
}
