<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "daerah_rawan".
 *
 * @property integer $id_daerah_rawan
 * @property double $longitude
 * @property double $latitude
 * @property string $nama
 * @property string $warna
 * @property string $isi
 */
class DaerahRawan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daerah_rawan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['longitude', 'latitude'], 'number'],
            [['nama', 'warna', 'isi'], 'required'],
            [['nama', 'isi'], 'string', 'max' => 50],
            [['warna'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_daerah_rawan' => 'Id Daerah Rawan',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'nama' => 'Nama',
            'warna' => 'Warna',
            'isi' => 'Isi',
        ];
    }
}
