<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id_user
 * @property string $nik
 * @property string $nama
 * @property string $alamat
 * @property string $telepon
 * @property string $username
 * @property string $password
 * @property string $foto
 */
class UserNew extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    //begin identityinterface
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['password']);
        
        return $fields;
    }

    public static function findIdentity($id_user)
    {
        return static::findOne($id_user);
    }

    public function validatePassword($username, $password)
    {
        // Use default validating or define yourself 
        return $password === self::findByUsername($username)['password'];
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id_user;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    //end identityinterface

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['nik', 'telepon'], 'string', 'max' => 20],
            [['nama', 'username'], 'string', 'max' => 50],
            [['alamat', 'password', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'nik' => 'Nik',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'telepon' => 'Telepon',
            'username' => 'Username',
            'password' => 'Password',
            'foto' => 'Foto',
        ];
    }
}
