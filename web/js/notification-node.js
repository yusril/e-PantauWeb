$( document ).ready(function() {
    
        var socket = io.connect('http://localhost:8890'); //'http://madamita.ml:8890');
    
        socket.on('emergency', function (data) {
    
            var content = JSON.parse(data);
    
            $.notify({
                // options
                message: "Lokasi Darurat baru<br>Dikirim dari<br>Lokasi: " + content.latitude + ", " + content.longitude
            },{
                // settings
                type: 'danger'
            });
    
        });
    
    });