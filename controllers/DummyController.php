<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\User;

class DummyController extends Controller
{
    public function actionIndex(){
        $password = Yii::$app->request->get('p');
        $hash = Yii::$app->getSecurity()->generatePasswordHash($password);
        return $hash;
    }

    public function actionValidate(){
        $password = Yii::$app->request->get('p');
        $hash = Yii::$app->request->get('h');
        if (Yii::$app->getSecurity()->validatePassword($password, $hash)) {
            // all good, logging user in
            return ":)";
        } else {
            // wrong password
            return ":'(";
        }
    }

    public function actionTestLogin()
    {
        $currentUser = User::findLevelById(Yii::$app->user->getId());
        return $currentUser;
    }

    public function actionGet()
    {
        //return var_dump($_GET);
        return var_dump(Yii::$app->request->queryParams);
    }

    public function actionGeoMap()
    {
        $aContext = array(
            'http' => array(
                // 'proxy' => 'proxy3.pens.ac.id:3128',
                'request_fulluri' => true,
            ),
        );
        $cxContext = stream_context_create($aContext);
        $json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=-7.235295,112.609799&key=AIzaSyCzqQVUqwMobrF8egv7JCpTXGL42f5EoJw', FALSE, $cxContext);
        $obj = json_decode($json);
        return var_dump($obj->results[0]->formatted_address);
    }
}
