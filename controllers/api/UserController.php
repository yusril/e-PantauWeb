<?php
namespace app\controllers\api;
use Yii;;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
use yii\web\UploadedFile;
use app\models\UserNew;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\UserNewSearch';
    
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'except' => ['view'],
            'auth' => function ($username, $password) {
                $user = UserNew::findByUsername($username);
                if ($user && $user->validatePassword($username, $password)) {
                    return $user;
                }
            }
        ];
        return $behaviors;
    }

    public function actionSearch()
    {
        if (!empty($_GET)) {
            $model = new $this->modelClass;
            foreach ($_GET as $key => $value) {
                if (is_array($value))
                foreach ($value as $keyVal => $subValue) {
                    if (!$model->hasAttribute($keyVal)) {
                        throw new \yii\web\HttpException(404, 'Invalid attribute:' . $keyVal);
                    }
                }
            }
            try {
                $provider = $model->search(Yii::$app->request->queryParams);/*new ActiveDataProvider([
                    'query' => $model->find()->where($_GET),
                    'pagination' => false
                ]);*/
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }

            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            throw new \yii\web\HttpException(400, 'There are no query string');
        }
    }

    public function actionUpload()
    {
        $file = UploadedFile::getInstanceByName("image");
        $name = \Yii::$app->request->post('name');
        if (empty($file) || empty($name)){
            return "Post request not sufficient";
        }
        $fileName = $name.".".$file->getExtension();
        $path = "file/profile/".$fileName; //Generate your save file path here;
        $status = $file->saveAs($path); //Your uploaded file is saved, you can process it further from here
        if ($status) {
            $user = UserNew::findByUsername($name);
            if(!empty($user)){
                $user->setAttribute('foto', $fileName);
                $user->save();
                return "Succeed";
            } else {
                return "Username not found";
            }
        } else {
            return $file->error();
        }
    }
}