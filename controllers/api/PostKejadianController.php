<?php
namespace app\controllers\api;
use yii\rest\ActiveController;
use yii\web\UploadedFile;
use app\models\PostKejadian;
class PostKejadianController extends ActiveController
{
    public $modelClass = 'app\models\PostKejadian';
    
    public function actionUpload()
    {
        $file = UploadedFile::getInstanceByName("image");
        $name = \Yii::$app->request->post('name');
        if (empty($file) || empty($name)){
            return "Post request not sufficient";
        }
        $fileName = time()."-".$name.".".$file->getExtension();
        $path = "file/post/".$fileName; //Generate your save file path here;
        $status = $file->saveAs($path); //Your uploaded file is saved, you can process it further from here
        if ($status) {
            $id_post = \Yii::$app->request->post('id_post');
            $postKejadian = PostKejadian::findById($id_post);
            $postKejadian->setAttribute('gambar', $fileName);
            $postKejadian->save();
            return "Succeed";
        } else {
            return $file->error();
        }
    }
}