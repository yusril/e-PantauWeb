<?php
namespace app\controllers\api;
use yii\rest\ActiveController;

class LaporanUserController extends ActiveController
{
    public $modelClass = 'app\models\LaporanUser';
}