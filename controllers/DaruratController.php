<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\LokasiDarurat;
use yii\helpers\Json;

class DaruratController extends Controller
{
    public $enableCsrfValidation = false;
public function actionSubmit()
    {
        $fordebug = "works!";
        if (Yii::$app->request->post()) {
    
            $name = Yii::$app->request->post('name');
            $latitude = Yii::$app->request->post('latitude');
            $longitude = Yii::$app->request->post('longitude');

            //missing: authentication
    
            //var_dump($message["lat"]." | ".$message["long"]);

            if(null != $name){
                $lokasiDarurat = new LokasiDarurat;
                $lokasiDarurat->id_user=$name;
                $lokasiDarurat->latitude=$latitude;
                $lokasiDarurat->longitude=$longitude;
                $lokasiDarurat->tanggal=date("Y-m-d H:i:s");
                $lokasiDarurat->save(false);
                $fordebug = $lokasiDarurat->errors;
            }
            else { $fordebug = "messagenull"; }

            return Yii::$app->redis->executeCommand('PUBLISH', [
                'channel' => 'emergency',
                'message' => Json::encode(['name' => $name, 'latitude' => $latitude, 'longitude' => $longitude])
            ]);
    
        }
    
        return $fordebug;
    }
}